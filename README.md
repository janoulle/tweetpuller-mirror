##TweetPuller
My first Chrome extension is available on the Chrome Webstore here: http://bit.ly/tweetpuller. 
<a href="http://bit.ly/tweetpuller" title="TweetPuller by Jane Ullah">TweetPuller</a> is a simple tool to display the 
first 10 - 40 tweets for a given twitter user. Currently at version 0.4, you can now view 
several tweets from different users in the same session, use the "Options" pages increase the 
default number of tweets seen and create a 'saved' user. The program should also give you helpful messages when you try to view a proected user's page or a 
nonexistent user's page.  Feedback and suggestions for improvement are welcome here: https://groups.google.com/d/forum/tweetpuller


##TODO:
Allow users to authenticate in order to view protected tweets.

##Problems?
Tweet me (@janetalkstech) with any problems. This is very much a learning experience so bear with me. :)

v0.4 changes:
Switched to Event Delegation method of handling event listeners